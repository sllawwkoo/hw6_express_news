import express, { Application } from "express";
import morgan from "morgan";
import bodyParser from "body-parser";
import { MainController } from "./controllers";
import Routes from "./routes";
import { DatabaseService } from "./services";
import dotenv from "dotenv";
import cors from "cors";

const PORT = process.env.PORT || 8000;

// cors опції, вказуеємо, які ресурси мають доступ до сервера
const CORS_OPTIONS = {
	origin: "http://localhost:3000",
	optionsSuccessStatus: 200,
};

class App {
	private app: Application;
	private mainController: MainController;

	constructor() {
		// ініціалізація express
		this.app = express();

		this.mainController = new MainController();
	}

	// маршрутизація
	routing() {
		this.app.get("/", this.mainController.getStartPage);

		// маршрутизація для API
		Object.keys(Routes).forEach((key) => {
			this.app.use(`/api/${key}`, Routes[key]);
		});
	}

	// ініціалізація плагінів які працюються як міддлвари
	initPlugins() {
		this.app.use(bodyParser.json());
		this.app.use(morgan("dev"));
		this.app.use(cors(CORS_OPTIONS));
	}

	// запуск сервера
	async start() {
		if (process.env.NODE_ENV !== "production") {
			//await DatabaseService.dropTables()
			await DatabaseService.createTables();
		}

		this.initPlugins();
		this.routing();

		this.app.listen(PORT, () => {
			console.log(`Server is running on port ${PORT}`);
		});
	}
}

// ініціалізація dotenv
dotenv.config();

// створення нашого сервера
const app = new App();

// запуск сервера
app.start();
