import { Router } from "express";
import { BaseController } from "../controllers/base.controller";

class BaseRouter {
	public router: Router; // Екземпляр роутера
	private controller: BaseController; // Контролер, який обробляє запити

	constructor(controller: BaseController) {
		this.router = Router(); // Ініціалізація роутера
		this.controller = controller; // Ініціалізація контролера

		this.routes(); // Виклик методу для налаштування маршрутів
	}

	// Налаштування маршрутів
	routes() {
		// POST і GET запити на кореневий шлях
		this.router
			.route("/")
			.post(this.controller.create) // Обробка POST запиту на створення запису
			.get(this.controller.getAll); // Обробка GET запиту на отримання всього списку записів
			// .get(this.controller.getList); // Обробка GET запиту на отримання списку записів з пагінацією

		// GET, PUT і DELETE запити з ідентифікатором
		this.router
			.route("/:id")
			.get(this.controller.getSingle) // Обробка GET запиту на отримання одного запису за ідентифікатором
			.put(this.controller.update) // Обробка PUT запиту на оновлення запису
			.delete(this.controller.delete); // Обробка DELETE запиту на видалення запису
	}
}

export default BaseRouter;
