import { NewsController } from "../controllers"; // Імпорт контролера для постів
import BaseRouter from "./base.routes"; // Імпорт базового роутера

class NewsRouter extends BaseRouter {
	constructor() {
		super(new NewsController()); // Виклик конструктора базового роутера з контролером для постів
	}
}

const { router } = new NewsRouter(); 

export default router; 