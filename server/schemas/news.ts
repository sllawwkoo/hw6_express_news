export default {
	name: "news",
	fields: [
		{
			name: "id",
			type: "increments",
		},
		{
			name: "title",
			type: "string",
		},
		{
			name: "text",
			type: "string",
		},
		{
			name: "createDate",
			type: "datetime",
		},
	],
};
