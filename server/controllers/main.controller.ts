import { Request, Response } from "express";

export class MainController {
	public getStartPage(req: Request, res: Response) {
		res.send("All News");
	}
}
