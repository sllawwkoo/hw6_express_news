import { BaseController } from "./base.controller"; // Імпорт класу BaseController з файлу base.controller

export class NewsController extends BaseController {
	// Створення класу NewsController, що розширює BaseController
	constructor() {
		super("news"); // Виклик конструктора базового класу з параметром 'users'
	}
}
