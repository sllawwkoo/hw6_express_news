import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

export function News() {
	const [news, setNews] = useState([]);

	useEffect(() => {
		const loadNews = async () => {
			const response = await fetch("http://localhost:8000/api/news");
			const data = await response.json();
			setNews(data);
		};

		loadNews();
	}, []);

	return (
		<div className="container">
			<div className="titleBox">
				<h1>All News</h1>
				<Link to="newsCreateUpdate">
					<button>Create New Article</button>
				</Link>
			</div>
			<div className="newsGrid">
				{news.data &&
					news.data.map((item, index) => (
						<Link
							to={`/news/${item.id}`}
							key={index}
							className="newsCard"
						>
							<h2 className="newsTitle">{item.title}</h2>
							<p className="newsText">{item.text}</p>
						</Link>
					))}
			</div>
		</div>
	);
}
