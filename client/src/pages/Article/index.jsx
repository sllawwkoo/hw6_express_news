import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";

export function Article() {
	const { newsId } = useParams();

	const [article, SetArticle] = useState({});

	useEffect(() => {
		const loadNews = async () => {
			const response = await fetch(`http://localhost:8000/api/news/${newsId}`, {
				method: "GET",
				headers: {
					"Content-Type": "application/json",
				},
			});
			const data = await response.json();
			SetArticle(data);
		};

		loadNews();
	}, [newsId]);

	console.log(article);

	return (
		<div className="container">
			<div className="article">
				{article && article.data && (
					<>
						<h2 className="articleTitle">{article.data.title}</h2>
						<p className="articleText">{article.data.text}</p>
					</>
				)}
			</div>
			<div className="buttons">
				<Link to={`/newsCreateUpdate/${newsId}`}>
					<button className="update">Update Article</button>
				</Link>
				<Link to="/">
					<button className="back2">Back to News</button>
				</Link>
			</div>
		</div>
	);
}
