import { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";

export function NewsCreateUpdate() {
	const [title, setTitle] = useState("");
	const [text, setText] = useState("");

	const { articleId } = useParams();
	console.log(articleId);

	const handleCreateSubmit = async (e) => {
		// e.preventDefault();
		try {
			const response = await fetch("http://localhost:8000/api/news", {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify({ title: title, text: text }),
			});
			const data = await response.json();
			console.log(data); // Виведення відповіді в консоль
		} catch (error) {
			console.error("Error:", error);
		}
	};

	const handleUpdateSubmit = async (e) => {
		e.preventDefault();
		try {
			const response = await fetch(
				`http://localhost:8000/api/news/${articleId}`,
				{
					method: "PUT",
					headers: {
						"Content-Type": "application/json",
					},
					body: JSON.stringify({ title: title, text: text }),
				}
			);
			const data = await response.json();
			console.log(data); // Виведення відповіді в консоль
		} catch (error) {
			console.error("Error:", error);
		}
	};

	useEffect(() => {
		const loadNews = async () => {
			const response = await fetch(
				`http://localhost:8000/api/news/${articleId}`,
				{
					method: "GET",
					headers: {
						"Content-Type": "application/json",
					},
				}
			);
			const data = await response.json();
			console.log(data);
			if (articleId) {
				setTitle(data.data.title);
				setText(data.data.text);
			}
		};

		loadNews();
	}, [articleId]);

	return (
		<div className="container">
			<div className="wrapperForma">
				{articleId ? <h1>Update News</h1> : <h1>Create News</h1>}
				<form onSubmit={articleId ? handleUpdateSubmit : handleCreateSubmit}>
					<label>
						Title:
						<input
							type="text"
							value={title}
							onChange={(e) => setTitle(e.target.value)}
							required
						/>
					</label>
					<label>
						Text:
						<input
							type="text"
							value={text}
							onChange={(e) => setText(e.target.value)}
							required
						/>
					</label>
					<button type="submit">Submit</button>
				</form>
			</div>
			<Link to="/">
				<button className="back">Bact to News</button>
			</Link>
		</div>
	);
}
